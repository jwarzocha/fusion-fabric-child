var quotes = [
  'SAVING means not spending your money straight away, but putting it away so you can spend it later. Usually people put their savings in a bank account, to keep the money safe until they have enough to buy what they want.',
  'A BUDGET is a plan on how you will spend the money you earn. It includes how much money you earn (and from where you earn it) and details what you want to spend your money on and how much you will save.',
  'A BANK is a place that looks after people’s money for them and keeps it safe. It also lends money to people to help them buy things like houses.',
  'INTEREST is money the bank pays you for letting them look after your money. It’s kind of like a reward for you not spending your money. The more money you put in the bank and the longer you leave it there for, the more interest you’ll get.',
  'An ATM is a short way to say "Automated Teller Machine". It’s a special machine that has money inside it and lets the bank’s customers make cash withdrawals and check their account balances without the need to go into the branch (or Bank shop).',
  'A PIN (or Personal Identification Number) is like a special secret password (made up of numbers) that you input into an ATM to tell the bank that its OK to do the transaction you are asking for. It is very important that you don’t tell your PIN to other people. If you do, they can take your money.'
]

function newQuote() {
  var randomNumber = Math.floor(Math.random() * (quotes.length));
  document.getElementById('quoteDisplay').textContent = quotes[randomNumber];
}