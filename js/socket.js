var socket = io();
const nerfPrice = 129.99;

socket.on('get-request', function(msg){
  console.log(msg);
  document.getElementById("modal-text").textContent= msg.testing;
  var Modalelem = document.querySelector('.modal');
  var instance = M.Modal.init(Modalelem);
  instance.open();
});

socket.on('set-amount-onaccount', function(amount, tranzakcje){
  console.log(11111111111)
  console.log(amount)
  document.getElementById("money-amount").textContent= "$"+amount.toFixed(2);
  let goalPercentage = ((amount/nerfPrice)*100).toFixed(0);
  let styleText = "width: "+goalPercentage+"%";
  document.getElementById("goal-progress").setAttribute("style", styleText);

  console.log(tranzakcje);
  
  let innerHtmlTable = ""; 
  tranzakcje.forEach(tranzakcja => {
    if(tranzakcja.transactionType == "expense")
      innerHtmlTable = innerHtmlTable + "<tr> <td>"+tranzakcja.date+"</td><td>School store</td><td>-$"+tranzakcja.amount.toFixed(2)+"</td></tr>"
    else
      innerHtmlTable = innerHtmlTable + "<tr> <td>"+tranzakcja.date+"</td><td>Pocket money</td><td>$"+tranzakcja.amount.toFixed(2)+"</td></tr>"
    
  });
  document.getElementById("table-body").innerHTML = innerHtmlTable;
});

socket.on('parent-is-ready', function(){
  console.log("parent is ready");
  var Modalelem = document.querySelector('#modal1');
  var instance = M.Modal.init(Modalelem);
  console.log(instance.isOpen)
  if(instance.isOpen) instance.close();
  console.log(instance.isOpen)
  var Modalelem2 = document.querySelector('#modal2');
  var instance2 = M.Modal.init(Modalelem2);
  console.log(instance2.isOpen)
  if(!instance2.isOpen) instance2.open();
  console.log(instance2.isOpen)
});

socket.on('parent-approved-payment', function(){
  console.log("parent-approved-payment");
  socket.emit('set-amount');  
  var Modalelem = document.querySelector('#modal3');
  var instance = M.Modal.init(Modalelem);
  console.log(instance.isOpen)
  if(!instance.isOpen) instance.open();
  console.log(instance.isOpen)
});

socket.on('declined', function(){
  console.log("declined-payment");
  var Modalelem = document.querySelector('#modal1');
  var instance = M.Modal.init(Modalelem);
  console.log(instance.isOpen)
  if(instance.isOpen) instance.close();
  console.log(instance.isOpen)
  var Modalelem2 = document.querySelector('#modal2');
  var instance2 = M.Modal.init(Modalelem2);
  console.log(instance2.isOpen)
  if(instance2.isOpen) instance2.close();
  console.log(instance2.isOpen)
  var Modalelem4 = document.querySelector('#modal4');
  var instance4 = M.Modal.init(Modalelem4);
  console.log(instance4.isOpen)
  if(!instance4.isOpen) instance4.open();
  console.log(instance4.isOpen)  
});

document.getElementById("i-will-pay").addEventListener("click", function() {
  console.log("parent not ready")
  fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/inquiry/press-button",{
    mode: 'no-cors'
  }).then((resp) => {
    if (resp.status !== 200) {
      throw new Error(resp.statusText);
    }
    return resp.json();
  }).then((data) => {
    console.log(data);
  }).catch((err) => {
    console.error('Error:', err.message);
  });
  // document.getElementById("modal-text").textContent= 'child-waits-for-readiness of parent';
  var Modalelem = document.querySelector('#modal1');
  var instance = M.Modal.init(Modalelem);
  console.log(instance.isOpen)
  if(!instance.isOpen) instance.open();
  console.log(instance.isOpen)
  socket.emit('check-if-parent-ready');
});


document.getElementById("i-pay-2").addEventListener("click", function() {
  console.log("i pay")
  fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/transaction/15.0",{
    mode: 'no-cors'
  }).then((resp) => {
    if (resp.status !== 200) {
      throw new Error(resp.statusText);
    }
    return resp.json();
  }).then((data) => {
    console.log(data);
  }).catch((err) => {
    console.error('Error:', err.message);
  });
  socket.emit('i-pay');
});