function renderChart(data, labels) {
  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'line',
      responsive: true,
      data: {
          labels: labels,
          datasets: [{
              label: 'This month',
              data: data,
              borderColor: '#694ED6',
              backgroundColor: '#D9D3F5',
          }]
      },
      options: {
        legend: {
          display: false,
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              max: 50,
            }
          }]
        }
      }
  });
}

document.addEventListener('DOMContentLoaded', function() {
  // fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/childaccount/status",{
  //   mode: 'no-cors'
  // }).then((resp) => {
  //   console.log(resp);
  //   if (resp.status !== 200) {
  //     throw new Error(resp.statusText);
  //   }
  //   return resp.json();
  // }).then((data) => {
  //   console.log(data);
  //   document.getElementById("money-amount").textContent = "$" + data.amount;
  // }).catch((err) => {
  //   console.error('Error:', err.message);
  // });
  // nav menu
  const menus = document.querySelectorAll('.side-menu');
  M.Sidenav.init(menus, {edge: 'right'});
  // add recipe form
  const forms = document.querySelectorAll('.side-form');
  M.Sidenav.init(forms, {edge: 'left'});

  // render sample data on chart js
  data = [20, 40, 10, 30];
  labels =  ["01/12/19", "08/12/19", "15/12/19", "22/12/19", "29/12/19"];
  renderChart(data, labels);
});