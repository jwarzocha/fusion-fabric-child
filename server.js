//stawianie serwera
var express = require('express');
var	app = express();
var	server = require('http').createServer(app);
const bodyParser = require('body-parser');
const helmet = require('helmet');  
var port = process.env.PORT || 2000;
var io = require('socket.io')(server);
const fetch = require("node-fetch");

app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept');
	next();
});

// //pre-flight requests
// app.options('*', function(req, res) {
// 	res.send(200);
// 	// res.send('<p>some html</p>');
// });

function compare(a, b) {
  const bandA = a.date;
  const bandB = b.date;

  let comparison = 0;
  if (bandA > bandB) {
    comparison = 1;
  } else if (bandA < bandB) {
    comparison = -1;
  }
  return comparison;
}

io.on('connection', function(socket){
	// socket.on('connection', function(){
	// 	io.emit('set-amount-onaccount');
	// });
	fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/childaccount/status",{
    mode: 'no-cors'
  }).then((resp) => {
    console.log(resp.status)
    if (resp.status !== 200) {
      throw new Error(resp.statusText);
    }
    return resp.json();
  }).then((data) => {
    console.log(data);
		fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/childaccount/transactions",{
				mode: 'no-cors'
			}).then((resp2) => {
				if (resp2.status !== 200) {
					throw new Error(resp2.statusText);
				}
				return resp2.json();
			}).then((tranzakcje) => {
				console.log(data);
				console.log(tranzakcje.completedTransactionList.sort(compare).reverse());
				// document.getElementById("money-amount").textContent = data.amount;
				io.emit('set-amount-onaccount', data.amount, tranzakcje.completedTransactionList.sort(compare).reverse());
			}).catch((err2) => {
				console.error('Error:', err2.message);
			});
  }).catch((err) => {
    console.error('Error:', err.message);
  });

	socket.on('set-amount', function(){
		fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/childaccount/status",{
			mode: 'no-cors'
		}).then((resp) => {
			console.log(resp.status)
			if (resp.status !== 200) {
				throw new Error(resp.statusText);
			}
			return resp.json();
		}).then((data) => {
			fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/childaccount/transactions",{
				mode: 'no-cors'
			}).then((resp2) => {
				if (resp2.status !== 200) {
					throw new Error(resp2.statusText);
				}
				return resp2.json();
			}).then((tranzakcje) => {
				console.log(data);
				console.log(tranzakcje.completedTransactionList.sort(compare).reverse());
				// document.getElementById("money-amount").textContent = data.amount;
				io.emit('set-amount-onaccount', data.amount, tranzakcje.completedTransactionList.sort(compare).reverse());
			}).catch((err2) => {
				console.error('Error:', err2.message);
			});
		}).catch((err) => {
			console.error('Error:', err.message);
		});
	});

	socket.on('check-if-parent-ready', function(){
		let intervalId;
		intervalId = setInterval(function() {
			console.log("status");
			fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/transactionwindow/status",{
				mode: 'no-cors'
			}).then((resp) => {
				if (resp.status !== 200) {
					throw new Error(resp.statusText);
				}
				return resp.json();
			}).then((data) => {
				console.log(data);
				if (data.status == "Approved"){
					io.emit('parent-is-ready');
					clearInterval(intervalId);
				}
				else if (data.status == "Declined"){
					io.emit('declined');
					clearInterval(intervalId);
				}
			}).catch((err) => {
				console.error('Error:', err.message);
			});				
		}, 5000);
	});

	socket.on('i-pay', function(){
		let intervalId;
		intervalId = setInterval(function() {
			console.log("status 2");
			fetch("https://fusion-fabric-parent-backend.herokuapp.com/payments/transaction/permissions/status",{
				mode: 'no-cors'
			}).then((resp) => {
				if (resp.status !== 200) {
					throw new Error(resp.statusText);
				}
				return resp.json();
			}).then((data) => {
				console.log(data);
				if (data.status == "Approved"){
					io.emit('parent-approved-payment');
					clearInterval(intervalId);
				}
				else if (data.status == "Declined. Not enough funds."){
					io.emit('declined');
					clearInterval(intervalId);
				}
			}).catch((err) => {
				console.error('Error:', err.message);
			});				
		}, 5000);
	});

	// setInterval(function() {
	// 	console.log("magic");
	// 	fetch("https://api.kanye.rest").then((resp) => {
	// 		if (resp.status !== 200) {
	// 			throw new Error(resp.statusText);
	// 		}
	// 		return resp.json();
	// 	}).then((data) => {
	// 		console.log(data);
	// 	}).catch((err) => {
	// 		console.error('Error:', err.message);
	// 	});
	// }, 3000);

	app.get('/get', (err, res) => {
		res.status(200);
		res.json({ working: true });
		//TODO: delete console.log
		console.log(res.req.body);
		res.end();
		io.emit('get-request', res.req.body);
	});

	//TODO: probably not needed
	// app.post('/post', (err, res) => {
	// 	res.status(200);
	// 	res.send('working');
	// 	//TODO: delete console.log
	// 	console.log(res.req.body);
	// 	// res.redirect('/pages/about.html');
	// 	res.end();
	// });

	// //TODO: put - 99% not needed
	// app.put('/put', (err, res) => {
	// 	res.status(200);
	// 	res.send('working');
	// 	//TODO: delete console.log
	// 	console.log(res.req.body);
	// 	res.end();
	// });



  console.log('a user connected');
});


server.listen(port, (err) => {
	if (err) {
		throw err;
	}
	/* eslint-disable no-console */
	console.log('Server is running(port 2000 on localhost)');
});
	
app.use(express.static('./'));
//wyświetla nam index.html
app.get('/', function(req, res){
	res.sendfile(__dirname + '/index.html');
});



